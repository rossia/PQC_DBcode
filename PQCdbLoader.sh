#!/bin/bash

WORKDIR="/home/alrossi/PQC_DBcode/"
DONEDIR="/home/alrossi/PQC_DBcode/Loaded/"
NEWDIR="/home/alrossi/PQC_DBcode/New/"
ISSUEDIR="/home/alrossi/PQC_DBcode/Issue/"
LOGDIR="/home/alrossi/PQC_DBcode/Logs/"


count=`ls -1 ${NEWDIR}*.zip 2>/dev/null | wc -l`
if [ $count == 0 ]
then
    echo "No files to be processed"
    exit
else
    echo "${count} batch(es) to be processed"
fi



#cd $WORKDIR
files=`ls ${NEWDIR}*.zip`
#echo "Following batches will be processed:"
#echo $files
for filename in $files;do
    echo ""
    echo $filename
    batch=`basename ${filename} | sed 's/.zip//g'`
    echo $batch
    echo "unzip $NEWDIR/${batch}.zip -d $NEWDIR"
    unzip -q "${NEWDIR}/${batch}.zip" -d $NEWDIR
    python3 Conversion_txt_to_XML/Convert_txt_to_xml_v2.py $NEWDIR/$batch > "${LOGDIR}${batch}_conversion.log"
    echo "Conversion Done"
    echo "Loading on DB..."
    uploadStatus=1
    for file in "${NEWDIR}/$batch"/*.zip; do
	if [ -f "$file" ]; then
	    if ! echo "$file" | grep -q "Final";then
		echo "$file"
		upload=`python3 ${WORKDIR}cmsdbldr/src/main/python/cmsdbldr_client.py --login --url=https://cmsdca.cern.ch/trk_loader/trker/cmsr "$file"`
		`echo $upload >> "${LOGDIR}${batch}_dbLoad.log"`
		dbC=`echo $upload | grep "200"`
		if [ "$dbC" = "" ]; then
		    echo "Loading of ${file} Failed"
		    uploadStatus=0
		fi
	    fi
	fi
    done

    if [ $uploadStatus == 0 ]; then
	echo "Loading Failed"
	mv "${NEWDIR}/${batch}" $ISSUEDIR	    
	mv "${NEWDIR}/${batch}.zip" $ISSUEDIR
    else
	echo "Loading Completed successfully"
	echo "Cleaning files..."
	mv "${NEWDIR}/${batch}.zip" "$DONEDIR/${batch}.zip"
	rm -rf "${NEWDIR}/${batch}"
    fi
done
		
